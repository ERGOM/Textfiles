! BioCGT processes file
! *********************
! properties of processes:
! name=           fortran variable name for the rate
! description=    e.g. "grazing of zooplankton"
! turnover=       fortran formula for calculating the process turnover [mol/kg or mol/m2]
! equation=       equation which, like a chemical equation, lists reaction agents and products of this process.
!                   example: t_no3 + 1/16*t_po4 -> t_lpp
!                   tracers to the left of the "->" are consumed, tracers to the right of the "->" are produced by this process.
! feedingEfficiency= name of an auxiliary variable (values 0..1) which tells how much of the food in a certain depth is accessible for the predator with vertLoc=FIS. Relevant for vertLoc=FIS only. Default="1.0"
! isActive=       1=active (default); 0=process is switched off
! isOutput=       1=occurs as output in model results; 0=internal use only (default)
! limitation=     TYPE tracer > value else otherProcess
! limitation=     TYPE tracer < value else otherProcess
!                   TYPE = HARD (theta function), MM (Michaelis-Menten), MMQ (quadratic Michaelis-Menten), IV (Ivlev), IVQ (quadratic Ivlev), LIN (linear), TANH (tangens hyperbolicus)
!                   tracer = name of tracer that needs to be present
!                   value = value that needs to be exceeded, may also be a constant or auxiliary
!                   otherProcess = process that takes place instead if this process gets hampered by the availability of "tracer"
!                 several of these lines may exist, the order of them may be relevant for the rate of "otherProcess".
! processType=    type of process, e.g. "propagation", default="standard"
! repaint=        number n of repainting actions to be done by the process, default=0
!                 This line is followed by n lines of this kind:
!   <oldColor> <element> = <newColor>    e.g.: "all  N   = blue "
!                                              "blue P   = none "
!                                              "red  all = green"
! vertLoc=        WAT=z-dependent (default), SED=in the sediment only, SUR=in the surface only, FIS=fish-type behaviour
! comment=        comment, default=""
!
! Process rates are calculated in the given order.
! *************************************************************************************
name        = p_n2_stf_down
description = downward nitrogen flux through the surface
turnover    = w_n2_stf*(n2_sat-t_n2)*theta(n2_sat-t_n2)*cgt_density
comment     = relaxation of nitrogen concentration against saturation at surface using \\piston-velocity, downward oxygen flow
equation    =  -> t_n2
vertLoc     = SUR
processType = gas_exchange
***********************
name        = p_n2_stf_up
description = upward nitrogen flux through the surface
turnover    = w_n2_stf*(t_n2-n2_sat)*theta(t_n2-n2_sat)*cgt_density
comment     = relaxation of nitrogen concentration against saturation at surface using piston-velocity, upward oxygen flow
equation    = t_n2 -> 
vertLoc     = SUR
processType = gas_exchange
***********************
name        = p_o2_stf_down
description = downward oxygen flux through the surface
turnover    = w_o2_stf*(o2_sat-t_o2)*theta(o2_sat-t_o2)*cgt_density
comment     = relaxation of oxygen concentration against saturation at surface using piston-velocity, downward oxygen flow
equation    =  -> t_o2
vertLoc     = SUR
processType = gas_exchange
***********************
name        = p_o2_stf_up
description = upward oxygen flux through the surface
turnover    = w_o2_stf*(t_o2-o2_sat)*theta(t_o2-o2_sat)*cgt_density
comment     = relaxation of oxygen concentration against saturation at surface using piston-velocity, upward oxygen flow
equation    = t_o2 -> 
vertLoc     = SUR
processType = gas_exchange
***********************
name        = p_co2_stf_down
description = downward co2 flux through the surface
turnover    = w_co2_stf*(patm_co2-pco2)*k0_co2*theta(patm_co2-pco2)*cgt_density
comment     = relaxation of co2 concentration against saturation at surface using piston-velocity, downward co2 flow
equation    =  -> t_dic
vertLoc     = SUR
processType = gas_exchange
***********************
name        = p_co2_stf_up
description = upward co2 flux through the surface
turnover    = w_co2_stf*(pco2-patm_co2)*k0_co2*theta(pco2-patm_co2)*cgt_density
comment     = relaxation of co2 concentration against saturation at surface using piston-velocity, upward co2 flow
equation    = t_dic -> 
vertLoc     = SUR
processType = gas_exchange
***********************
name        = p_no3_assim_lpp
description = assimilation of nitrate by large-cell phytoplankton
turnover    = lpp_plus_lpp0*lr_assim_lpp*t_no3/(din+epsilon)
equation    = 1.1875*h3oplus + 6.4375*h2o + rfr_c*t_dic + rfr_p*t_po4 + t_no3 -> 8.625*t_o2 + t_lpp
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_nh4_assim_lpp
description = assimilation of ammonium by large-cell phytoplankton
turnover    = lpp_plus_lpp0*lr_assim_lpp*t_nh4/(din+epsilon)
equation    = t_nh4 + rfr_p*t_po4 + rfr_c*t_dic + 7.4375*h2o -> t_lpp + 6.625*t_o2 + 0.8125*h3oplus
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_no3_assim_spp
description = assimilation of nitrate by small-cell phytoplankton
turnover    = spp_plus_spp0*lr_assim_spp*t_no3/(din+epsilon)
equation    = 1.1875*h3oplus + 6.4375*h2o + rfr_c*t_dic + rfr_p*t_po4 + t_no3 -> 8.625*t_o2 + t_spp
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_nh4_assim_spp
description = assimilation of ammonium by small-cell phytoplankton
turnover    = spp_plus_spp0*lr_assim_spp*t_nh4/(din+epsilon)
equation    = t_nh4 + rfr_p*t_po4 + rfr_c*t_dic + 7.4375*h2o -> t_spp + 6.625*t_o2 + 0.8125*h3oplus
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_nh4_assim_lip
description = assimilation of ammonium by limnic phytoplankton
turnover    = lip_plus_lip0*lr_assim_lip*t_nh4/(din+epsilon)
equation    = 7.4375*h2o + rfr_c*t_dic + rfr_p*t_po4 + t_nh4 -> 0.8125*h3oplus + 6.625*t_o2 + t_lip
processType = BGC/pelagic/phytoplankton
***********************
name        = p_no3_assim_lip
description = assimilation of nitrate by limnic phytoplankton
turnover    = lip_plus_lip0*lr_assim_lip*t_no3/(din+epsilon)
equation    = t_no3 + rfr_p*t_po4 + rfr_c*t_dic + 6.4375*h2o + 1.1875*h3oplus -> t_lip + 8.625*t_o2
processType = BGC/pelagic/phytoplankton
***********************
name        = p_n2_assim_cya
description = fixation of dinitrogen by diazotroph cyanobacteria
turnover    = cya_plus_cya0*lr_assim_cya
equation    = 3*rfr_p*h3oplus + 0.5*t_n2 + rfr_p*t_po4 + rfr_c*t_dic + 7.9375*h2o -> t_cya + 7.375*t_o2
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_assim_lpp_doc
description = Production of DOC by LIP
turnover    = rfr_c * t_lpp * lr_assim_lpp_doc
comment     = CO2 + H2O -> (CH2O) + O2
equation    = h2o + t_dic -> t_o2 + t_doc
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_assim_spp_doc
description = Production of DOC by SPP
turnover    = rfr_c * t_spp * lr_assim_spp_doc
comment     = CO2 + H2O -> (CH2O) + O2
equation    = t_dic + h2o -> t_doc + t_o2
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_assim_lip_doc
description = Production of DOC by LPP
turnover    = rfr_c * t_lip * lr_assim_lip_doc
equation    = h2o + t_dic -> t_o2 + t_doc
processType = BGC/pelagic/phytoplankton
***********************
name        = p_assim_cya_doc
description = Production of DOC by CYA
turnover    = rfr_c * t_cya * lr_assim_cya_doc
comment     = CO2 + H2O -> (CH2O) + O2
equation    = h2o + t_dic -> t_o2 + t_doc
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_assim_lpp_dop
description = Production of DOP by LPP
turnover    = rfr_p * t_lpp * lr_assim_lpp_dop
comment     = 106CO2 + PO4(3-) + 106H2O +3H3O(1+) -> (CH2O)106H3PO4 + 106O2 + 3H2O
equation    = 106*t_dic + t_po4 + 106*h2o + 3*h3oplus -> t_dop + 106*t_o2 + 3*h2o
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_assim_spp_dop
description = Production of DOP by SPP
turnover    = rfr_p * t_spp * lr_assim_spp_dop
comment     = 106CO2 + PO4(3-) + 106H2O +3H3O(1+) -> (CH2O)106H3PO4 + 106O2 + 3H2O
equation    = 3*h3oplus + 106*h2o + t_po4 + 106*t_dic -> 3*h2o + 106*t_o2 + t_dop
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_assim_lip_dop
description = Production of DOP by LIP
turnover    = rfr_p * t_lip * lr_assim_lip_dop
equation    = 106*t_dic + t_po4 + 106*h2o + 3*h3oplus -> t_dop + 106*t_o2 + 3*h2o
processType = BGC/pelagic/phytoplankton
***********************
name        = p_nh4_assim_lpp_don
description = Production of DON by LPP
turnover    = t_lpp * lr_assim_lpp_don*t_nh4/(din+epsilon)
equation    = ohminus + 6.625*H2O + t_nh4 + rfr_c*t_dic -> H2O + 6.625*t_o2 + t_don
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_no3_assim_lpp_don
description = Production of DON by LPP
turnover    = t_lpp * lr_assim_lpp_don*t_no3/(din+epsilon)
equation    = rfr_c*t_dic + t_no3 + 6.625*H2O + h3oplus -> t_don + 8.625*t_o2
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_nh4_assim_spp_don
description = Production of DON by SPP
turnover    = t_spp * lr_assim_spp_don*t_nh4/(din+epsilon)
equation    = rfr_c*t_dic + t_nh4 + 6.625*H2O + ohminus -> t_don + 6.625*t_o2 + H2O
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_no3_assim_spp_don
description = Production of DON by SPP
turnover    = t_spp * lr_assim_spp_don*t_no3/(din+epsilon)
equation    = h3oplus + 6.625*H2O + t_no3 + rfr_c*t_dic -> 8.625*t_o2 + t_don
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_nh4_assim_lip_don
description = Production of DON by LIP
turnover    = t_lip * lr_assim_lip_don*t_nh4/(din+epsilon)
equation    = rfr_c*t_dic + t_nh4 + 6.625*H2O + ohminus -> t_don + 6.625*t_o2 + H2O
processType = BGC/pelagic/phytoplankton
***********************
name        = p_no3_assim_lip_don
description = Production of DON by LIP
turnover    = t_lip * lr_assim_lip_don*t_no3/(din+epsilon)
equation    = h3oplus + 6.625*H2O + t_no3 + rfr_c*t_dic -> 8.625*t_o2 + t_don
processType = BGC/pelagic/phytoplankton
***********************
name        = p_poc_resp
description = respiration of POC
turnover    = t_poc * r_poc_rec * exp(q10_det_rec * cgt_temp)
comment     = (CH2O) + O2 -> CO2 + H2O
equation    = t_o2 + t_poc -> h2o + t_dic
processType = BGC/pelagic/phytoplankton
limitation  = IV t_o2 > o2_min_det_resp
***********************
name        = p_poc_denit
description = recycling of POC using nitrate (denitrification)
turnover    = t_poc*r_poc_rec*exp(q10_det_rec*cgt_temp)
comment     = 106(CH2O)(H3PO4) + 84.8 NO3 + 84.8 H3O ->\\106CO2 + 106 H2O + 42.4 N2 + 127.2 H2O
equation    = 0.8*h3oplus + 0.8*t_no3 + t_poc -> 0.4*t_n2 + 2.2*h2o + t_dic
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 > no3_min_det_denit
***********************
name        = p_poc_sulf
description = Mineralization of POC, e-acceptor sulfate (sulfate reduction)
turnover    = t_poc*r_poc_rec*exp(q10_det_rec*cgt_temp)
comment     = 106(CH2O) + 53 SO4 + 106 H3O -> 106 CO2 + 212 H2O + 53 H2S
equation    = h3oplus + 0.5*so4 + t_poc -> 2*h2o + 0.5*t_h2s + t_dic
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 < no3_min_det_denit
***********************
name        = p_pocp_resp
description = respiration of POCP
turnover    = t_pocp * lr_pocp * exp(q10_det_rec * cgt_temp)
comment     = (CH2O)106H3PO4 + 106O2 + 3H2O -> 106CO2 + PO4(3-) + 106H2O + 3H3O(1+)
equation    = 3*H2O + t_pocp + 106*t_o2 -> 3*h3oplus + 106*H2O + t_po4 + 106*t_dic
isOutput    = 1
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 > o2_min_det_resp
***********************
name        = p_pocp_denit
description = recycling of POC using nitrate (denitrification)
turnover    = t_pocp*r_pocp_rec*exp(q10_det_rec*cgt_temp)
comment     = 106(CH2O)(H3PO4) + 84.8 NO3 + 84.8 H3O + 3OH(1-)->\\106CO2 + 106 H2O + 42.4 N2 + 127.2 H2O + PO4 +3H2O
equation    = 3*ohminus + 84.8*h3oplus + 84.8*t_no3 + t_pocp -> t_po4 + 42.4*t_n2 + 236.2*H2O + 106*t_dic
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 > no3_min_det_denit
***********************
name        = p_pocp_sulf
description = Mineralization of POC, e-acceptor sulfate (sulfate reduction)
turnover    = t_pocp*r_pocp_rec*exp(q10_det_rec*cgt_temp)
comment     = 106(CH2O) + 53 SO4 + 106 H3O + 3OH(1-) -> 106 CO2 + 212 H2O + 53 H2S + 3H2O
equation    = 3*ohminus + 106*h3oplus + 53*so4 + t_pocp -> t_po4 + 53*t_h2s + 215*h2o + 106*t_dic
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 < no3_min_det_denit
***********************
name        = p_pocn_resp
description = respiration of POCN
turnover    = t_pocn * lr_pocn * exp(q10_det_rec * cgt_temp)
equation    = t_pocn + 6.625*t_o2 + 0.5*h3oplus -> 6.625*t_dic + t_nh4 + 6.625*H2O + 0.5*ohminus
isOutput    = 1
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 > o2_min_det_resp
***********************
name        = p_pocn_denit
description = recycling of POCN using nitrate (denitrification)
turnover    = t_pocn*r_pocn_rec*exp(q10_det_rec*cgt_temp)
comment     = (CH2O)106(NH3)16 + 84.8NO3(1-) + 92.8H3O(1+) - > 106CO2 + 16NH4(1+) + 42.4N2 + 233.2H2O + \\8OH(1-)
equation    = t_pocn + 5.3*t_no3 + 5.8*h3oplus -> 6.625*t_dic + t_nh4 + 2.65*t_n2 + 14.575*H2O + 0.5*ohminus
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 > no3_min_det_denit
***********************
name        = p_pocn_sulf
description = Mineralization of POCN, e-acceptor sulfate (sulfate reduction)
turnover    = t_pocn*r_pocn_rec*exp(q10_det_rec*cgt_temp)
equation    = 7.125*h3oplus + 3.3125*SO4 + t_pocn -> 0.5*ohminus + 13.25*H2O + 3.3125*t_h2s + t_nh4 + 6.625*t_dic
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 < no3_min_det_denit
***********************
name        = p_lpp_graz_zoo
description = grazing of zooplankton eating large-cell phytoplankton
turnover    = (t_zoo+zoo0)*lr_graz_zoo*t_lpp/max(food_zoo,epsilon)
equation    = t_lpp -> t_zoo
processType = BGC/pelagic/zooplankton
***********************
name        = p_spp_graz_zoo
description = grazing of zooplankton eating small-cell phytoplankton
turnover    = (t_zoo+zoo0)*lr_graz_zoo*t_spp/max(food_zoo,epsilon)
equation    = t_spp -> t_zoo
processType = BGC/pelagic/zooplankton
***********************
name        = p_cya_graz_zoo
description = grazing of zooplankton eating diazotroph cyanobacteria
turnover    = (t_zoo+zoo0)*lr_graz_zoo*(0.5*t_cya)/max(food_zoo,epsilon)
equation    = t_cya -> t_zoo
processType = BGC/pelagic/zooplankton
***********************
name        = p_lip_graz_zoo
description = grazing of zooplankton eating limnic phytoplankton
turnover    = (t_zoo+zoo0)*lr_graz_zoo*t_lip/max(food_zoo,epsilon)
equation    = t_lip -> t_zoo
processType = BGC/pelagic/zooplankton
***********************
name        = p_lpp_resp_nh4
description = respiration of large-cell phytoplankton
turnover    = t_lpp*r_lpp_resp
equation    = 0.8125*h3oplus + 6.625*t_o2 + t_lpp -> 7.4375*h2o + rfr_c*t_dic + rfr_p*t_po4 + (1-don_fraction)*t_nh4 + don_fraction*t_don
processType = BGC/pelagic/phytoplankton
***********************
name        = p_spp_resp_nh4
description = respiration of small-cell phytoplankton
turnover    = t_spp*r_spp_resp
equation    = t_spp + 6.625*t_o2 + 0.8125*h3oplus -> don_fraction*t_don + (1-don_fraction)*t_nh4 + rfr_p*t_po4 + rfr_c*t_dic + 7.4375*h2o
processType = BGC/pelagic/phytoplankton
***********************
name        = p_lip_resp_nh4
description = respiration of limnic phytoplankton
turnover    = t_lip*r_lip_resp
equation    = t_lip + 6.625*t_o2 + 0.8125*h3oplus -> don_fraction*t_don + (1-don_fraction)*t_nh4 + rfr_p*t_po4 + rfr_c*t_dic + 7.4375*h2o
processType = BGC/pelagic/phytoplankton
***********************
name        = p_cya_resp_nh4
description = respiration of diazotroph cyanobacteria
turnover    = t_cya*r_cya_resp
equation    = t_cya + 6.625*t_o2 + 0.8125*h3oplus -> (1-don_fraction)*t_nh4 + don_fraction*t_don + rfr_p*t_po4 + rfr_c*t_dic + 7.4375*h2o
processType = BGC/pelagic/phytoplankton
***********************
name        = p_zoo_resp_nh4
description = respiration of zooplankton
turnover    = zoo_eff*r_zoo_resp
equation    = t_zoo + 6.625*t_o2 + 0.8125*h3oplus -> don_fraction*t_don + (1-don_fraction)*t_nh4 + rfr_p*t_po4 + rfr_c*t_dic + 7.4375*h2o
processType = BGC/pelagic/zooplankton
***********************
name        = p_lpp_mort_det
description = mortality of large-cell phytoplankton
turnover    = t_lpp*r_pp_mort*(1+9*theta(5.0e-6-t_o2))
equation    = t_lpp -> t_det
processType = BGC/pelagic/phytoplankton
***********************
name        = p_spp_mort_det
description = mortality of small-scale phytoplankton
turnover    = t_spp*r_pp_mort*(1+9*theta(5.0e-6-t_o2))
equation    = t_spp -> t_det
processType = BGC/pelagic/phytoplankton
***********************
name        = p_lip_mort_det
description = mortality of limnic phytoplankton
turnover    = t_lip*r_pp_mort*(1+9*theta(5.0e-6-t_o2))
equation    = t_lip -> t_det
processType = BGC/pelagic/phytoplankton
***********************
name        = p_cya_mort_det
description = mortality of diazotroph cyanobacteria
turnover    = t_cya*r_pp_mort*(1+9*theta(5.0e-6-t_o2))
equation    = t_cya -> t_det
processType = BGC/pelagic/phytoplankton
***********************
name        = p_cya_mort_det_diff
description = mortality of diazotroph cyanobacteria due to strong turbulence
turnover    = t_cya*r_pp_mort*(r_cya_mort_diff*theta(cgt_diffusivity-r_cya_mort_thresh))
equation    = t_cya -> t_det
isOutput    = 1
processType = BGC/pelagic/phytoplankton
***********************
name        = p_zoo_mort_det
description = mortality of zooplankton
turnover    = zoo_eff*r_zoo_mort*(1+9*theta(5.0e-6-t_o2))
equation    = t_zoo -> t_det
processType = BGC/pelagic/zooplankton
***********************
name        = p_nh4_nit_no3
description = nitrification
turnover    = t_nh4*r_nh4_nitrif*exp(q10_nit*cgt_temp)
equation    = h2o + 2*t_o2 + t_nh4 -> 2*h3oplus + t_no3
processType = BGC/pelagic/reoxidation
***********************
name        = p_det_resp_nh4
description = recycling of detritus using oxygen (respiration)
turnover    = t_det*r_det_rec*exp(q10_det_rec*cgt_temp)
equation    = 0.8125*h3oplus + 6.625*t_o2 + t_det -> 7.4375*h2o + rfr_c*t_dic + rfr_p*t_po4 + t_nh4
isOutput    = 1
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 > o2_min_det_resp
***********************
name        = p_det_denit_nh4
description = recycling of detritus using nitrate (denitrification)
turnover    = t_det*r_det_rec*exp(q10_det_rec*cgt_temp)
comment     = as long as there is enough nitrate, no H2S("negative \\oxygen") is generated (2 NO3- + \\2 H3O+ -> N2 + 3 H2O + 5/2 O2)
equation    = t_det + 5.3*t_no3 + 6.1125*h3oplus -> 2.65*t_n2 + 15.3875*h2o + t_nh4 + rfr_p*t_po4 + rfr_c*t_dic
isOutput    = 1
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 > no3_min_det_denit
***********************
name        = p_det_sulf_nh4
description = recycling of detritus using sulfate (sulfate reduction)
turnover    = t_det*r_det_rec*exp(q10_det_rec*cgt_temp)
equation    = t_det + 3.3125*so4 + 7.4375*h3oplus -> t_nh4 + rfr_p*t_po4 + rfr_c*t_dic + 3.3125*t_h2s + 14.0625*h2o
isOutput    = 1
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 < no3_min_det_denit
***********************
name        = p_sed_resp_nh4
description = recycling of sedimentary detritus to ammonium using oxygen (respiration)
turnover    = lr_sed_rec*sed_active
equation    = 0.8125*h3oplus + 6.625*t_o2 + t_sed -> 7.4375*h2o + rfr_c*t_dic + rfr_p*t_po4 + t_nh4
vertLoc     = SED
isOutput    = 1
processType = BGC/benthic/mineralisation
***********************
name        = p_sed_denit_nh4
description = recycling of sedimentary detritus to ammonium using nitrate (denitrification)
turnover    = lr_sed_rec*sed_active
comment     = as long as there is enough nitrate, no "negative oxygen" is \\generated (2 NO3- + 2 \\H3O+ -> N2 + 3 H2O + 5/2 O2)
equation    = 5.3*t_no3 + 6.1125*h3oplus + t_sed -> 15.3875*h2o + 2.65*t_n2 + t_nh4 + rfr_p*t_po4 + rfr_c*t_dic
vertLoc     = SED
processType = BGC/benthic/mineralisation
limitation  = HARD t_o2 < 0.0
limitation  = MMQ t_no3 > no3_min_sed_denit
***********************
name        = p_sed_sulf_nh4
description = recycling of sedimentary detritus to ammonium using sulfate (sulfate reduction)
turnover    = lr_sed_rec*sed_active
equation    = 7.4375*h3oplus + 3.3125*so4 + t_sed -> 14.0625*h2o + 3.3125*t_h2s + rfr_c*t_dic + rfr_p*t_po4 + t_nh4
vertLoc     = SED
processType = BGC/benthic/mineralisation
limitation  = HARD t_o2 < 0.0
limitation  = MMQ t_no3 < no3_min_sed_denit
***********************
name        = p_sed_poc_resp
description = recycling of sedimentary poc to dic using oxygen (respiration)
turnover    = lr_sed_poc_rec*poc_active
equation    = t_o2 + t_sed_poc -> h2o + t_dic
vertLoc     = SED
processType = BGC/benthic/mineralisation
***********************
name        = p_sed_poc_denit
description = recycling of sedimentary poc to dic using nitrate (denitrification)
turnover    = lr_sed_poc_rec*poc_active
equation    = t_sed_poc + 0.8*h3oplus + 0.8*t_no3 -> t_dic + 0.4*t_n2 + 2.2*h2o
vertLoc     = SED
processType = BGC/benthic/mineralisation
limitation  = HARD t_o2 < 0.0
limitation  = MMQ t_no3 > no3_min_sed_denit
***********************
name        = p_sed_poc_sulf
description = recycling of sedimentary poc to dic using sulfate (sulfate reduction)
turnover    = lr_sed_poc_rec*poc_active
equation    = t_sed_poc + 0.5*so4 + h3oplus -> t_dic + 0.5*t_h2s + 2.0*h2o
vertLoc     = SED
processType = BGC/benthic/mineralisation
limitation  = HARD t_o2 < 0.0
limitation  = MMQ t_no3 < no3_min_sed_denit
***********************
name        = p_po4_retent_ips
description = retention of phosphate in the sediment under oxic conditions
turnover    = p_sed_resp_nh4*frac_po4retent
equation    = rfr_p*fe3plus + rfr_p*t_po4 -> rfr_p*t_ips
vertLoc     = SED
processType = BGC/benthic/P_retention
limitation  = MMQ t_o2 > o2_min_po4_retent
***********************
name        = p_ips_liber_po4
description = liberation of phosphate from the sediment under anoxic conditions
turnover    = t_ips*r_ips_liber
comment     = FePO4 -> PO43- + Fe3+
equation    = t_ips -> t_po4 + fe3plus
vertLoc     = SED
processType = BGC/benthic/P_retention
limitation  = HARD t_h2s > h2s_min_po4_liber
***********************
name        = p_h2s_oxo2_sul
description = oxidation of hydrogen sulfide with oxygen
turnover    = t_h2s*t_o2*k_h2s_o2*exp(q10_h2s*cgt_temp)
equation    = t_h2s + 0.5*t_o2 -> t_sul + h2o
processType = BGC/pelagic/reoxidation
***********************
name        = p_h2s_oxno3_sul
description = oxidation of hydrogen sulfide with nitrate
turnover    = t_h2s*t_no3*k_h2s_no3*exp(q10_h2s*cgt_temp)
equation    = 0.4*h3oplus + 0.4*t_no3 + t_h2s -> 0.2*t_n2 + 1.6*h2o + t_sul
isOutput    = 1
processType = BGC/pelagic/reoxidation
***********************
name        = p_sul_oxo2_so4
description = oxidation of elemental sulfur with oxygen
turnover    = t_sul*t_o2*k_sul_o2*exp(q10_h2s*cgt_temp)
equation    = 3*h2o + 1.5*t_o2 + t_sul -> 2*h3oplus + so4
processType = BGC/pelagic/reoxidation
***********************
name        = p_sul_oxno3_so4
description = oxidation of elemental sulfur with nitrate
turnover    = t_sul*t_no3*k_sul_no3*exp(q10_h2s*cgt_temp)
equation    = 1.2*h2o + 1.2*t_no3 + t_sul -> 0.6*t_n2 + 0.8*h3oplus + so4
processType = BGC/pelagic/reoxidation
***********************
name        = p_det_sedi_sed
description = detritus sedimentation
turnover    = (1.0-erosion_is_active)*(0.0-w_det_sedi)*t_det*cgt_density
equation    = t_det -> t_sed
vertLoc     = SED
processType = physics/sedimentation
***********************
name        = p_ipw_sedi_ips
description = sedimentation of iron PO4
turnover    = (1.0-erosion_is_active)*(0.0-w_ipw_sedi)*t_ipw*cgt_density
equation    = t_ipw -> t_ips
vertLoc     = SED
processType = physics/sedimentation
***********************
name        = p_poc_sedi_sed
description = poc sedimentation
turnover    = (1.0-erosion_is_active)*(0.0-w_poc_var)*t_poc*cgt_density
equation    = t_poc -> t_sed_poc
vertLoc     = SED
processType = physics/sedimentation
***********************
name        = p_pocn_sedi_sed
description = pocn sedimentation
turnover    = (1.0-erosion_is_active)*(0.0-w_pocn_sedi)*t_pocn*cgt_density
equation    = t_pocn -> t_sed_pocn
vertLoc     = SED
processType = physics/sedimentation
***********************
name        = p_pocp_sedi_sed
description = pocp sedimentation
turnover    = (1.0-erosion_is_active)*(0.0-w_pocp_sedi)*t_pocp*cgt_density
equation    = t_pocp -> t_sed_pocp
vertLoc     = SED
processType = physics/sedimentation
***********************
name        = p_sed_ero_det
description = sedimentary detritus erosion
turnover    = erosion_is_active*r_sed_ero*sed_active
equation    = t_sed -> t_det
vertLoc     = SED
processType = physics/erosion
***********************
name        = p_ips_ero_ipw
description = erosion of iron PO4
turnover    = erosion_is_active*r_ips_ero*t_ips
equation    = t_ips -> t_ipw
vertLoc     = SED
processType = physics/erosion
***********************
name        = p_sed_ero_poc
description = sedimentary poc erosion
turnover    = erosion_is_active*r_sed_ero*poc_active
equation    = t_sed_poc -> t_poc
vertLoc     = SED
processType = physics/erosion
***********************
name        = p_sed_ero_pocn
description = sedimentary pocn erosion
turnover    = erosion_is_active*r_sed_ero*pocn_active
equation    = t_sed_pocn -> t_pocn
vertLoc     = SED
processType = physics/erosion
***********************
name        = p_sed_ero_pocp
description = sedimentary pocp erosion
turnover    = erosion_is_active*r_sed_ero*pocp_active
equation    = t_sed_pocp -> t_pocp
vertLoc     = SED
processType = physics/erosion
***********************
name        = p_sed_biores_det
description = bio resuspension of sedimentary detritus
turnover    = r_biores*exp(-0.02*cgt_bottomdepth)*sed_active
equation    = t_sed -> t_det
vertLoc     = SED
processType = BGC/benthic/bioresuspension
limitation  = MMQ t_o2 > o2_min_sed_resp
***********************
name        = p_ips_biores_ipw
description = bio resuspension of iron PO4
turnover    = r_biores*exp(-0.02*cgt_bottomdepth)*t_ips
equation    = t_ips -> t_ipw
vertLoc     = SED
processType = BGC/benthic/bioresuspension
limitation  = MMQ t_o2 > o2_min_sed_resp
***********************
name        = p_sed_biores_poc
description = bio resuspension of sedimentary poc
turnover    = r_biores*exp(-0.02*cgt_bottomdepth)*poc_active
equation    = t_sed_poc -> t_poc
vertLoc     = SED
isOutput    = 1
processType = BGC/benthic/bioresuspension
limitation  = MMQ t_o2 > o2_min_sed_resp
***********************
name        = p_sed_biores_pocn
description = bio resuspension of sedimentary pocn
turnover    = r_biores*exp(-0.02*cgt_bottomdepth)*pocn_active
equation    = t_sed_pocn -> t_pocn
vertLoc     = SED
processType = BGC/benthic/bioresuspension
limitation  = MMQ t_o2 > o2_min_sed_resp
***********************
name        = p_sed_biores_pocp
description = bio resuspension of sedimentary pocp
turnover    = r_biores*exp(-0.02*cgt_bottomdepth)*pocp_active
equation    = t_sed_pocp -> t_pocp
vertLoc     = SED
processType = BGC/benthic/bioresuspension
limitation  = MMQ t_o2 > o2_min_sed_resp
***********************
name        = p_sed_burial
description = burial of detritus deeper than max_sed
turnover    = (sed_tot-sed_tot_burial)/cgt_timestep*t_sed/sed_tot
equation    = t_sed -> 
vertLoc     = SED
isOutput    = 1
processType = physics/parametrization_deep_burial
***********************
name        = p_ips_burial
description = burial of iron PO4
turnover    = fac_ips_burial*(sed_tot-sed_tot_burial)/cgt_timestep*t_ips/sed_tot
comment     = urspruengliche Version: ips_eff*r_ips_burial*sed_tot_active/rfr_c/sed_max
equation    = t_ips -> 
vertLoc     = SED
isOutput    = 1
processType = physics/parametrization_deep_burial
***********************
name        = p_poc_burial
description = burial of poc deeper than max_sed
turnover    = (sed_tot-sed_tot_burial)/cgt_timestep*t_sed_poc/sed_tot
equation    = t_sed_poc -> 
vertLoc     = SED
isOutput    = 1
processType = physics/parametrization_deep_burial
***********************
name        = p_pocn_burial
description = burial of pocn deeper than max_sed
turnover    = (sed_tot-sed_tot_burial)/cgt_timestep*t_sed_pocn/sed_tot
equation    = t_sed_pocn -> 
vertLoc     = SED
isOutput    = 1
processType = physics/parametrization_deep_burial
***********************
name        = p_pocp_burial
description = burial of pocp deeper than max_sed
turnover    = (sed_tot-sed_tot_burial)/cgt_timestep*t_sed_pocp/sed_tot
equation    = t_sed_pocp -> 
vertLoc     = SED
isOutput    = 1
processType = physics/parametrization_deep_burial
***********************
name        = p_sed_pocn_resp
description = recycling of sedimentary pocn to dic and NH4 using oxygen (respiration)
turnover    = lr_sed_rec*pocn_active
equation    = 0.5*h3oplus + t_sed_pocn + 6.625*t_o2 -> 0.5*ohminus + t_nh4 + 6.625*t_dic + 6.625*h2o
vertLoc     = SED
processType = BGC/benthic/mineralisation
***********************
name        = p_sed_pocp_resp
description = recycling of sedimentary pocp to dic and PO4 using oxygen (respiration)
turnover    = lr_sed_rec*pocp_active
equation    = 106*t_o2 + t_sed_pocp + 3*h2o -> 3*h3oplus + 106*t_dic + t_po4 + 106*h2o
vertLoc     = SED
processType = BGC/benthic/mineralisation
***********************
name        = p_sed_pocn_denit
description = recycling of sedimentary pocn to dic and NH4 using nitrate (denitrification)
turnover    = lr_sed_rec*pocn_active
equation    = 5.8*h3oplus + 5.3*t_no3 + t_sed_pocn -> 0.5*ohminus + 14.575*h2o + 2.65*t_n2 + t_nh4 + 6.625*t_dic
vertLoc     = SED
processType = BGC/benthic/mineralisation
limitation  = HARD t_o2 < 0.0
limitation  = MMQ t_no3 > no3_min_sed_denit
***********************
name        = p_sed_pocp_denit
description = recycling of sedimentary pocp to dic and PO4 using nitrate (denitrification)
turnover    = lr_sed_rec*pocp_active
equation    = 84.8*t_no3 + 84.8*h3oplus + 3*ohminus + t_sed_pocp -> 236.2*h2o + 42.4*t_n2 + t_po4 + 106*t_dic
vertLoc     = SED
processType = BGC/benthic/mineralisation
limitation  = HARD t_o2 < 0.0
limitation  = MMQ t_no3 > no3_min_sed_denit
***********************
name        = p_sed_pocn_sulf
description = recycling of sedimentary pocn to dic and NH4 using sulfate (sulfate reduction)
turnover    = lr_sed_rec*pocn_active
equation    = t_pocn + 3.3125*SO4 + 7.125*h3oplus -> 6.625*t_dic + t_nh4 + 3.3125*t_h2s + 13.25*H2O + 0.5*ohminus
vertLoc     = SED
processType = BGC/benthic/mineralisation
limitation  = HARD t_o2 < 0.0
limitation  = MMQ t_no3 < no3_min_sed_denit
***********************
name        = p_sed_pocp_sulf
description = recycling of sedimentary pocp to dic and PO4 using sulfate (sulfate reduction)
turnover    = lr_sed_rec*pocp_active
equation    = 3*ohminus + 106*h3oplus + 53*so4 + t_pocp -> t_po4 + 53*t_h2s + 215*h2o + 106*t_dic
vertLoc     = SED
processType = BGC/benthic/mineralisation
limitation  = HARD t_o2 < 0.0
limitation  = MMQ t_no3 < no3_min_sed_denit
***********************
name        = p_doc2pco
description = particle formation from DOC
turnover    = t_doc * r_doc2poc
equation    = t_doc -> t_poc
isOutput    = 1
***********************
name        = p_dop2pocp
description = particle formation from DOP
turnover    = t_dop * r_dop2pocp
equation    = t_dop -> t_pocp
isOutput    = 1
***********************
name        = p_don2pocn
description = particle formation from DON
turnover    = t_don * r_don2pocn
equation    = t_don -> t_pocn
isOutput    = 1
***********************
name        = p_doc_resp
description = respiration of DOC
turnover    = t_doc * r_doc_rec * exp(q10_doc_rec * cgt_temp)
comment     = (CH2O) + O2 -> CO2 + H2O
equation    = t_doc + t_o2 -> t_dic + h2o
isOutput    = 1
processType = BGC/pelagic/phytoplankton
limitation  = IV t_o2 > o2_min_det_resp
***********************
name        = p_doc_denit
description = recycling of DOC using nitrate (denitrification)
turnover    = t_doc*r_doc_rec*exp(q10_det_rec*cgt_temp)
comment     = 106(CH2O)(H3PO4) + 84.8 NO3 + 84.8 H3O ->\\106CO2 + 106 H2O + 42.4 N2 + 127.2 H2O
equation    = 0.8*h3oplus + 0.8*t_no3 + t_doc -> 0.4*t_n2 + 2.2*h2o + t_dic
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 > no3_min_det_denit
***********************
name        = p_doc_sulf
description = Mineralization of DOC, e-acceptor sulfate (sulfate reduction)
turnover    = t_doc*r_doc_rec*exp(q10_det_rec*cgt_temp)
comment     = 106(CH2O) + 53 SO4 + 106 H3O -> 106 CO2 + 212 H2O + 53 H2S
equation    = h3oplus + 0.5*so4 + t_doc -> 2*h2o + 0.5*t_h2s + t_dic
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 < no3_min_det_denit
***********************
name        = p_dop_resp
description = respiration of DOP
turnover    = t_dop * lr_dop * exp(q10_det_rec * cgt_temp)
comment     = (CH2O)106H3PO4 + 106O2 + 3H2O -> 106CO2 + PO4(3-) + 106H2O + 3H3O(1+)
equation    = 106*t_o2 + t_dop + 3*H2O -> 106*t_dic + t_po4 + 106*H2O + 3*h3oplus
isOutput    = 1
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 > o2_min_det_resp
***********************
name        = p_dop_denit
description = recycling of DOP using nitrate (denitrification)
turnover    = t_dop*r_dop_rec*exp(q10_det_rec*cgt_temp)
comment     = 106(CH2O)(H3PO4) + 84.8 NO3 + 84.8 H3O + 3OH(1-)->\\106CO2 + 106 H2O + 42.4 N2 + 127.2 H2O + PO4 +3H2O
equation    = 3*ohminus + 84.8*h3oplus + 84.8*t_no3 + t_dop -> t_po4 + 42.4*t_n2 + 236.2*H2O + 106*t_dic
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 > no3_min_det_denit
***********************
name        = p_dop_sulf
description = Mineralization of DOP, e-acceptor sulfate (sulfate reduction)
turnover    = t_dop*r_dop_rec*exp(q10_det_rec*cgt_temp)
comment     = 106(CH2O) + 53 SO4 + 106 H3O + 3OH(1-) -> 106 CO2 + 212 H2O + 53 H2S + 3H2O
equation    = t_dop + 53*so4 + 106*h3oplus + 3*ohminus -> 106*t_dic + 215*h2o + 53*t_h2s + t_po4
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 < no3_min_det_denit
***********************
name        = p_don_resp
description = respiration of DON
turnover    = t_don * lr_don * exp(q10_det_rec * cgt_temp)
equation    = t_don + 6.625*t_o2 + 0.5*h3oplus -> 6.625*t_dic + t_nh4 + 6.625*H2O + 0.5*ohminus
isOutput    = 1
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 > o2_min_det_resp
***********************
name        = p_don_denit
description = recycling of DON using nitrate (denitrification)
turnover    = t_don*r_don_rec*exp(q10_det_rec*cgt_temp)
comment     = (CH2O)106(NH3)16 + 84.8NO3(1-) + 92.8H3O(1+) - > 106CO2 + 16NH4(1+) + 42.4N2 + 233.2H2O + \\8OH(1-)
equation    = t_don + 5.3*t_no3 + 5.8*h3oplus -> 6.625*t_dic + t_nh4 + 2.65*t_n2 + 14.575*H2O + 0.5*ohminus
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 > no3_min_det_denit
***********************
name        = p_don_sulf
description = Mineralization of DON, e-acceptor sulfate (sulfate reduction)
turnover    = t_don*r_don_rec*exp(q10_det_rec*cgt_temp)
equation    = t_don + 3.3125*SO4 + 7.125*h3oplus -> 6.625*t_dic + t_nh4 + 3.3125*t_h2s + 13.25*H2O + 0.5*ohminus
processType = BGC/pelagic/mineralisation
limitation  = IV t_o2 < o2_min_det_resp
limitation  = IV t_no3 < no3_min_det_denit
***********************
name        = p_cdom_decay
description = decay of cdom due to light
turnover    = t_cdom*r_cdom_decay*cgt_light/r_cdom_light
equation    = t_cdom -> 
processType = BGC/pelagic/mineralisation
***********************
name        = p_alk_btf
description = calcium carbonate dissolution from till sediments
turnover    = alk_btf_0 * (alk_btf_sw_BS + alk_btf_0_BBfac*alk_btf_sw_BB)
comment     = artifical alkalinity bottom flux mimicing possible alkalinity from till sediments
equation    = caco3 + 2*h3oplus -> ca2plus + t_dic + 3*h2o
vertLoc     = SED
isOutput    = 1
processType = BGC/benthic/mineralisation
***********************
name        = p_nh4_nitdenit_n2
description = coupled nitrification and denitrification after mineralization of detritus in oxic sediments
turnover    = frac_denit_sed*(p_sed_resp_nh4+p_sed_pocn_resp)*theta(t_o2-5.0e-6)
comment     = NH4+ + 0.75O2 -> 0.5N2 + 0.5H2O + H3O+\\it is a highly parametrized shortcut due to 2D sediment. Indeed, denitrification requires organic carbon which is neglected in this formulation. It is somewhere hidden in the oxic mineralisation of sediment and in the \\parameter frac_denit_sed.
equation    = t_nh4 + 0.75*t_o2 -> 0.5*t_n2 + 0.5*h2o + h3oplus
vertLoc     = SED
processType = BGC/benthic/mineralisation
***********************
